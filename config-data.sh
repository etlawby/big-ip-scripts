#! /bin/bash

# config for dp1.secure-demo.net

source /usr/lib/bigstart/bigip-ready-functions

if [ ! -z "$(tmsh list sys provision one-line | egrep 'level' | egrep 'sys provision ltm')" ]; then
   echo "deprovisioning ltm"
   tmsh modify /sys provision ltm level none
   sleep 20
   echo "waiting for BIG-IP ready"
   wait_bigip_ready
   echo "BIG-IP is ready"

fi
if [[ "$(tmsh list sys provision one-line | egrep 'level' | egrep 'afm|avr|cgnat|gtm|ilx|pem' | wc -l)" -ne 6 ]]; then
   echo "provisioning modules"
   tmsh modify /sys provision afm cgnat pem gtm avr ilx level nominal
   sleep 20
   echo "waiting for BIG-IP ready"
   wait_bigip_ready
   echo "BIG-IP is ready"
   echo "provisioning completed"
else
   echo "provisioning ok"
fi

tmsh modify /sys global-settings hostname data.secure-demo.net 
tmsh create /net vlan subscriber-vlan interfaces replace-all-with { 1.1 } cmp-hash src-ip
tmsh create /net vlan internet-vlan interfaces replace-all-with { 1.2 } cmp-hash dst-ip
tmsh create /net self subscriber-self address 10.1.10.11/24 vlan subscriber-vlan 
tmsh create /net self internet-self address 10.1.20.11/24 vlan internet-vlan
tmsh create /net route default-tmm-route network default gw 10.1.20.1
tmsh create /net route cp1-internal-route network 10.1.21.0/25 gw 10.1.10.21
tmsh create /net route cp1-external-route network 10.1.21.128/25 gw 10.1.20.21
tmsh create /net route server-route network 10.1.101.0/24 gw 10.1.20.101
tmsh create /net route client-route network 10.1.30.0/24 gw 10.1.10.101

# install new images
# scp -i /config/ssh/ssh_host_rsa_key ubuntu@10.1.1.8:BIGIP-* /shared/images
# tmsh modify /sys disk directory /shared new-size 31457280
# tmsh save /sys config
# tmsh show /sys disk   
# tmsh reboot

# tmsh install /sys software image BIGIP-15.1.8-0.0.7.iso volume HD1.2 create-volume 
# tmsh install /sys software image BIGIP-16.1.3.2-0.0.4.iso volume HD1.3 create-volume 
# tmsh install /sys software image BIGIP-17.0.0.1-0.0.4.iso volume HD1.4 create-volume

# tmsh modify /ltm default-node-monitor rule icmp

tmsh create /ltm node google-dns-1-node address 8.8.4.4
tmsh create /ltm node google-dns-2-node address 8.8.8.8
tmsh create /ltm monitor dns dns-google-monitor defaults-from dns qname www.google.com
tmsh create /ltm pool google-dns-pool members replace-all-with { google-dns-1-node:53 google-dns-2-node:53 } monitor dns-google-monitor 
tmsh create /ltm pool internet-monitor-pool members replace-all-with { google-dns-1-node:0 google-dns-2-node:0 } monitor gateway_icmp

# tmsh create /ltm node self-mgmt-node address 10.1.1.245
# tmsh create /ltm pool self-monitor-pool members replace-all-with { self_mgmt_node:0 } monitor gateway_icmp

tmsh create /ltm virtual http-respond-vs destination 10.1.11.80:80 profiles add { http } rules { http-respond-irule internet-monitor-irule }

tmsh create /ltm virtual cgnat-vs destination 0.0.0.0:0 mask 0.0.0.0 rules { internet-monitor-irule }
tmsh create security nat source-translation pba-lsn { type dynamic-pat pat-mode pba addresses replace-all-with { 10.1.100.0/24 } ports replace-all-with { 1000-2000 } }
tmsh create /security nat policy pba-policy rules replace-all-with { pba-rule { translation { source pba-lsn } } }
tmsh modify /ltm virtual cgnat-vs security-nat-policy { policy pba-policy }

tmsh save /sys config
