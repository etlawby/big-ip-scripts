#! /bin/bash

## /ubuntu-client-boot.sh

# to install: curl -OLs https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/ubuntu-client-boot.sh; bash ubuntu-client-boot.sh

REPO="https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/"
INSTALL_DIR="/home/ubuntu/install-scripts/"
THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename ${BASH_SOURCE})"

main () {

  touch /home/ubuntu/.sudo_as_admin_successful
  sudo chown ubuntu:ubuntu /home/ubuntu/.sudo_as_admin_successful
  sudo chmod 755 ${THIS_SCRIPT}

  if [ "$(whoami)" != "ubuntu" ]; then
    echo "switching from $(whoami) to ubuntu"
  #  echo "please re-run script"
    sudo chown ubuntu:ubuntu ${THIS_SCRIPT}

    # automatically change to ubuntu user when using webshell
    echo "su - ubuntu" >> ~/.profile
  
    if [ ! -f /home/ubuntu/.profile-original ]; then
      cp -p /home/ubuntu/.profile /home/ubuntu/.profile-original
    fi
    echo "${THIS_SCRIPT}" >> /home/ubuntu/.profile
    su - ubuntu
  else

    if [ ! -z "$(cat /home/ubuntu/.profile | egrep ${THIS_SCRIPT})" ]; then
      mv /home/ubuntu/.profile-original /home/ubuntu/.profile
      echo "resetting ~/ubuntu/.profile"
    fi

    #reset if network is configured
    if [ -f ${INSTALL_DIR}ubuntu-server-network.sh ]; then
      ${INSTALL_DIR}ubuntu-client-network.sh disconnect
    fi

    mgmt_ip="$(ip addr | grep "inet 10\.1\.1\." | sed "s|^.*inet \(10\.1\.1\.[^/]*\).*$|\1|")"
    component_name="$(curl -Ls metadata.udf/deployment | jq --arg ip "${mgmt_ip}" '.deployment.components[] | select(.mgmtIp == $ip) | .name' | cut -d'"' -f2)"
    echo "${component_name} Boot Script starting"
    sudo hostnamectl set-hostname "${component_name}"

    sudo apt update
    sudo NEEDRESTART_SUSPEND=1 apt install nginx fcgiwrap wireguard wireguard-tools dnsmasq dnsutils tcpdump git jq -y

    if [ -z "$(grep "^200 " /etc/iproute2/rt_tables)" ]; then
      echo "adding mgmt route"
      echo "200 via_mgmt" | sudo tee -a /etc/iproute2/rt_tables > /dev/null
      sudo ip route add default via 10.1.1.1 table via_mgmt
      sudo ip rule add from 10.1.1.0/24 table via_mgmt
    fi

    mkdir -p ${INSTALL_DIR}
    cd ${INSTALL_DIR}
    curl -OLs "${REPO}ubuntu-client-dns-update.sh"
    curl -OLs "${REPO}ubuntu-client-network.sh"
    curl -OLs "${REPO}ubuntu-client-wireguard.sh"
    curl -OLs "${REPO}ubuntu-client-firefox.sh"
    curl -OLs "${REPO}ubuntu-update-certs.sh"
    curl -OLs "${REPO}ubuntu-demo-ui.sh"
    chmod 755 ${INSTALL_DIR}/*.sh

    ${INSTALL_DIR}ubuntu-client-dns-update.sh
    ${INSTALL_DIR}ubuntu-update-certs.sh

    # git clone demo-ui
    # deploy demo-ui

    echo "#aliases" > ~/alias.sh
    echo "alias connect=\"${INSTALL_DIR}ubuntu-client-network.sh connect\"" >> ~/alias.sh
    echo "alias disconnect=\"${INSTALL_DIR}ubuntu-client-network.sh disconnect\"" >> ~/alias.sh
    echo "alias upalias=\"source ~/alias.sh\"" >> ~/alias.sh

    ${INSTALL_DIR}ubuntu-client-firefox.sh
    ${INSTALL_DIR}ubuntu-client-network.sh connect
    ${INSTALL_DIR}ubuntu-client-network.sh disconnect
    ${INSTALL_DIR}ubuntu-client-wireguard.sh

  fi

}

# end of script:
# this enables main function to be at top of file

export command="$0 $*"
main "$@"; exit
