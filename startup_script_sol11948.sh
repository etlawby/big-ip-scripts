#!/bin/bash

# Automatic Licensing Script
# https://support.f5.com/csp/article/K11948
# Filename: /config/startup_script_sol11948.sh
# echo "/config/startup_script_sol11948.sh" >> /config/startup

# Run from /config/startup
# If license has less than LICENSE_DAYS_RENEW days validity, obtain registration key from:
# 1. next entry in EVAL_KEY_FILE
# 2. first line of DEV_KEY_FILE
# 3. Existing licence (for recyclable Dev keys)
# If licenced with EVAL_KEY, set cronjob to re-run this script every RETRY_INTERVAL minutes in case of invalid key
# Set cronjob to re-run this script every CHECK_INTERVAL hours in case of license expiry
# Log status to LOG_FILE
# Copy initial EVAL_KEYS_FILE to EVAL_KEYS_FILE_BACKUP

THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename $BASH_SOURCE)"
EVAL_KEY_FILE=/config/EVAL-KEYS
EVAL_KEY_FILE_BACKUP=/config/EVAL-KEYS-BACKUP
DEV_KEY_FILE=/config/DEV-KEYS
LOG_FILE=/var/log/relicence.log
RETRY_INTERVAL=15
#CHECK_INTERVAL=24
LICENSE_DAYS_RENEW=5
DEV_SERVER="authem.f5net.com"
PROD_SERVER="activate.f5.com"

check_route () {
   fqdn=$1
   ip=$2
   if [ -z $ip ]; then
      echo "DNS lookup for $fqdn failed"
      echo "$(date) - DNS lookup for $fqdn failed" >> $LOG_FILE
   else
      echo "$fqdn = $ip"
      ping=$(ping $fqdn -c 3 | tail -n 2 | head -n 1 | egrep "100% packet loss")
      if [ -z "$ping" ]; then
         echo "Route to $fqdn is OK"
      else
         echo "Route to $fqdn ($ip) FAILED"
         echo "$(date) - Route to $fqdn ($ip) FAILED" >> $LOG_FILE
      fi
   fi
}

echo "STARTING LICENSING SCRIPT $THIS_SCRIPT"
echo "$(date) - STARTING LICENSING SCRIPT $THIS_SCRIPT" >> $LOG_FILE

my_bigip_ready=$(bigstart status mcpd | egrep -v "run")
if [ ! -z $my_bigip_ready ]; then
   echo "Waiting for mcpd <${my_bigip_ready}>"
   echo "$(date) - Waiting for mcpd <${my_bigip_ready}>" >> $LOG_FILE
fi
source /usr/lib/bigstart/bigip-ready-functions
wait_bigip_ready license
if [ ! -z $my_bigip_ready ]; then
   my_bigip_ready=$(bigstart status mcpd | egrep -v "run")
   echo "mcpd is ready <${my_bigip_ready}>"
   echo "$(date) - mcpd is ready <${my_bigip_ready}>" >> $LOG_FILE
fi

# CRONTAB
   crontab -l > my_crontab_file
   cat my_crontab_file | egrep -v "$THIS_SCRIPT" > my_crontab2_file
   cp my_crontab2_file my_crontab_file
#   echo "*/30 * * * * $THIS_SCRIPT" >> my_crontab_file
#  script to run daily at 04:30
   echo "30 4 * * * $THIS_SCRIPT" >> my_crontab_file
   crontab my_crontab_file
   rm my_crontab_file
   echo "CRONTAB crontab updated (daily)"
   echo "$(date) - crontab updated (daily)" >> /var/log/relicence.log

license_nonoperational=$(tmsh show /sys license | egrep "Can't load license, may not be operational")
license_expired=$(tmsh show /sys license | egrep "Warning: license has expired")
license_enddate=$(tmsh show /sys license | egrep "License End Date")
#echo "1:$license_nonoperational"
#echo "2:$license_expired"
#echo "3:$license_enddate"

license_remaining=0
if [ ! -z "$license_nonoperational" ]; then
   license_valid=0
   echo "No license"
   echo "$(date) - No license" >> $LOG_FILE
elif [ ! -z "$license_expired" ]; then
   license_valid=0
   echo "License has expired"
   echo "$(date) - License has expired" >> $LOG_FILE
elif [ ! -z "$license_enddate" ]; then
   license_end=$(date -d $(echo $license_enddate | sed "s/License End Date[ ]*//") +%s)
   #echo "License end date: $(date -d @$license_end)"
   license_remaining=$( expr $license_end - $(date +%s) ) 
   #echo "Seconds remaining: $license_remaining"
   license_remaining=$( expr $license_remaining / 86400 )
   #echo "Days remaining: $license_remaining"
   if [[ "$license_remaining" -gt "$LICENSE_DAYS_RENEW" ]]; then
      license_valid=1
   else
      license_valid=0
      echo "License expiring in $license_remaining days"
      echo "$(date) - License expiring in $license_remaining days" >> $LOG_FILE
   fi
else
   license_valid=0
   echo "ERROR: License state cannot be determined"
   echo "$(date) - ERROR: License state cannot be determined" >> LOG_FILE
fi

if [[ "$license_valid" -eq "1" ]]; then
   echo "License valid: $license_remaining days remaining" 
   echo "$(date) - License valid: $license_remaining days remaining" >> $LOG_FILE
   exit 0
fi

echo "Starting relicensing"

if [ -f "$EVAL_KEY_FILE" ]; then
    echo "Eval key file exists: $EVAL_KEY_FILE"
    if [ -f "$EVAL_KEY_FILE_BACKUP" ]; then
       echo "Eval key backup exists: $EVAL_KEY_FILE_BACKUP exists"
    else
       echo "Creating new Eval key backup: $EVAL_KEY_FILE_BACKUP"
       cp $EVAL_KEY_FILE $EVAL_KEY_FILE_BACKUP
    fi 
    EVAL_KEY=$(head -n1 $EVAL_KEY_FILE)
    if [ -z $EVAL_KEY ]; then
       EVAL_KEY="none"
    fi
    echo "Eval key selected: $EVAL_KEY"
    keys_remaining=$(wc $EVAL_KEY_FILE  | sed 's/^ *//g'| cut -f1 -d " ")
    keys_remaining=$(expr $keys_remaining - 1 )
    echo "Eval keys remaining: $keys_remaining"
    if [[ $keys_remaining -gt 0 ]]; then
       tail -n $keys_remaining $EVAL_KEY_FILE_BACKUP > $EVAL_KEY_FILE 
    else
       rm -f $EVAL_KEY_FILE
    fi
else
    echo "Eval key file not found: $EVAL_KEY_FILE"
    EVAL_KEY="none"
fi

if [ -f "$DEV_KEY_FILE" ]; then
    echo "Dev key file exists: $DEV_KEY_FILE"
    DEV_KEY=$(head -n1 $DEV_KEY_FILE)
    if [ -z $DEVL_KEY ]; then
       DEVL_KEY="none"
    fi
    echo "Dev key selected: $DEV_KEY"
else
    echo "Dev key file not found: $DEV_KEY_FILE"
    DEV_KEY="none"
fi

if [ 1 ]; then
    TMSH_KEY=$(tmsh show /sys license | egrep "key" | cut -c 40-)
    if [ -z $TMSH_KEY ]; then
       TMSH_KEY="none"
    fi
    echo "Current license key: $TMSH_KEY"
fi

if [ $EVAL_KEY != "none" ]; then
   my_key=$EVAL_KEY
# ENHANCED CRONTAB
   crontab -l > my_crontab_file
   cat my_crontab_file | egrep -v "$THIS_SCRIPT" > my_crontab2_file
   cp my_crontab2_file my_crontab_file
   echo "*/$RETRY_INTERVAL * * * * $THIS_SCRIPT" >> my_crontab_file
   crontab my_crontab_file
   rm my_crontab_file
   echo "CRONTAB crontab updated (retry: $RETRY_INTERVAL mins)"
   echo "$(date) - crontab updated (retry: $RETRY_INTERVAL mins)" >> /var/log/relicence.log

elif [ $DEV_KEY != "none" ]; then
   my_key=$DEV_KEY
elif [ $TMSH_KEY != "none" ]; then
   my_key=$TMSH_KEY
else
   my_key="No key found"
   echo "$my_key: aborting relicencing"
fi

echo "Registration key selected: $my_key"
if [ $my_key != "No key found" ]; then

   ip_dev_server=$(dig +short $DEV_SERVER)
   ip_prod_server=$(dig +short $PROD_SERVER)

   tmsh create /sys management-route TEMPORARY-DEV-LICENSE_SERVER_ROUTE network $ip_dev_server/32 gateway 10.1.1.1
   tmsh create /sys management-route TEMPORARY-PROD-LICENSE_SERVER_ROUTE network $ip_prod_server/32 gateway 10.1.1.1

   check_route $DEV_SERVER $ip_dev_server
   check_route $PROD_SERVER $ip_prod_server

   echo "Installing registration key $my_key"
   echo "$(date) - Installing registration key $my_key" >> $LOG_FILE

   tmsh install /sys license registration-key $my_key
   sleep 20
   echo "waiting for bigip_ready"
   wait_bigip_ready
   echo "bigip is ready"
   
   tmsh delete /sys management-route TEMPORARY-DEV-LICENSE_SERVER_ROUTE
   tmsh delete /sys management-route TEMPORARY-PROD-LICENSE_SERVER_ROUTE

fi

echo "Script completed"
