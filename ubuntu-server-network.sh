#! /bin/bash

## ubuntu-server-network.sh connect|disconnect

NW_CONF="/etc/netplan/50-cloud-init.yaml"
RESOLVER="$(curl -Ls metadata.udf/deployment | jq '.deployment.components[] | select(.name == "client1") | .mgmtIp' | cut -d'"' -f2)"

main () {

  mgmt_ip="$(ip addr | grep "inet 10\.1\.1\." | sed "s|^.*inet \(10\.1\.1\.[^/]*\).*$|\1|")"
  traffic_ip="$(curl -Ls metadata.udf/deployment | jq --arg ip "${mgmt_ip}" '.deployment.components[] | select(.mgmtIp == $ip) | (.trafficIps[].primary)' | cut -d'"' -f2)"
  traffic_net="$(echo ${traffic_ip} | cut -d'.' -f1-3)"

#  echo "mgmt_ip=<${mgmt_ip}> traffic_ip=<${traffic_ip}> traffic_net=<${traffic_net}>"

  case "${traffic_net}" in
    "10.1.30")
        gateway="10.1.40.14"
        ;;
    "10.1.220")
        gateway="10.1.220.6"
        ;;
    *)
        gateway="10.1.20.11"
  esac
  echo "Gateway is ${gateway}"

  if [ "${1}" == "connect" ]; then
    cat << EOF | sudo tee ${NW_CONF} > /dev/null
### updated by script ~/ubuntu-server-network.sh connect
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
      dhcp4-overrides:
        use-dns: false
      nameservers:
        addresses: [${RESOLVER}]
    ens6:
      dhcp4: true
      dhcp4-overrides:
        use-dns: false
        use-routes: false
      routes:
        - to: 10.1.0.0/16
          via: ${gateway}
EOF

  elif [ "${1}" == "disconnect" ]; then
    cat << EOF | sudo tee "${NW_CONF}" > /dev/null
### updated by script ~/ubuntu-server-network.sh disconnect
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
    ens6: {}
EOF

  else 
    echo "usage: ${BASH_SOURCE[0]} connect|disconnect"
    exit 0 
  fi
  sudo netplan apply
}


# end of script:
# this enables main function to be at top of file

export command="$0 $*"
main "$@"; exit
