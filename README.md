# BIG-IP Scripts

To run:

curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/boot.sh > /config/boot.sh; nohup bash /config/boot.sh --INSTANCE N

INSTANCE=[data|control], N=1,2 (optional, for data only)


curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/boot-client.sh N > /home/ubuntu/boot.sh; source /home/ubuntu/boot.sh;

N=1,2 (optional)


curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/boot-server.sh N > /home/ubuntu/boot.sh; source /home/ubuntu/boot.sh;

N=1,2 (optional)


