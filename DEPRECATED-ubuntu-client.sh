#! /bin/bash

## ubuntu-client.sh (was client.sh)

NW_CONF="/etc/netplan/50-cloud-init.yaml"
CLIENT_IP_FILE="/home/ubuntu/client_ip"

main () {
  client_ip="$(cat "${CLIENT_IP_FILE}")"

  if [ "${1}" == "connect" ]; then

    cat << EOF | sudo tee ${NW_CONF} > /dev/null
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
    ens6:
      addresses:
        - ${client_ip}/24
      nameservers:
        addresses: [10.1.1.2]
      routes:
        - to: 0.0.0.0/0
          via: 10.1.10.11
EOF

  elif [ "${1}" == "disconnect" ]; then
    cat << EOF | sudo tee "${NW_CONF}" > /dev/null
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
    ens6: {}
EOF
  else 
    echo "usage: ${BASH_SOURCE[0]} connect|disconnect"
    exit 0 
  fi
  sudo netplan apply
}

# end of script:
# this enables main function to be at top of file

export command="$0 $*"
main "$@"; exit
