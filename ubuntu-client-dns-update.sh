#! /bin/bash

DNS_CONF="/etc/dnsmasq.conf"
LOCAL_DOMAIN="udf"

jumphost_dns="$(curl -Ls http://metadata.udf/deployment | jq '.deployment.components[] | (.name == "jumphost") | .mgmtIp"' | cut -d'"' -f2)"
if [ -n "$(dig +short @${jumphost_dns} client)" ]; then
  echo "using jumphost for authoratative DNS"
  exit 0
else
  bash -c "$(wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/install/20-configure-dns.sh)"
fi

cat << EOF | sudo tee ${DNS_CONF} > /dev/null
expand-hosts
domain-needed
domain=${LOCAL_DOMAIN}
local=/${LOCAL_DOMAIN}/
localise-queries
no-resolv
server=10.1.1.2
bind-interfaces
except-interface=nonexisting
no-dhcp-interface=lo
cache-size=10000
bogus-priv
# client specific
interface=ens5
EOF

sudo systemctl restart dnsmasq