#! /bin/bash

 # https://github.com/jlesage/docker-firefox

sudo apt  -y install \ca-certificates \curl \gnupg \lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo docker run -d \
    --name=firefox \
    --restart unless-stopped \
    -e KEEP_APP_RUNNING=1 \
    -e FF_OPEN_URL="" \
    -e FF_KIOSK=0 \
    -p 5800:5800 \
    -v /docker/appdata/firefox:/config:rw \
    --shm-size 2g \
    jlesage/firefox