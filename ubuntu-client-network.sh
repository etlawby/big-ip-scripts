#! /bin/bash

## ubuntu-client-network.sh connect|disconnect

NW_CONF="/etc/netplan/50-cloud-init.yaml"
RESOLVER="$(curl -Ls metadata.udf/deployment | jq '.deployment.components[] | select(.name == "server1") | .mgmtIp' | cut -d'"' -f2)"

main () {

  mgmt_ip="$(ip addr | grep "inet 10\.1\.1\." | sed "s|^.*inet \(10\.1\.1\.[^/]*\).*$|\1|")"
  traffic_ip="$(curl -Ls metadata.udf/deployment | jq --arg ip "${mgmt_ip}" '.deployment.components[] | select(.mgmtIp == $ip) | (.trafficIps[].primary)' | cut -d'"' -f2)"
  traffic_net="$(echo ${traffic_ip} | cut -d'.' -f1-3)"

#echo "mgmt_ip=<${mgmt_ip}> traffic_ip=<${traffic_ip}> traffic_net=<${traffic_net}>"

  case "${traffic_net}" in
    "10.1.30")
        gateway="10.1.30.14"
        ;;
    "10.1.160")
        gateway="10.1.160.5"
        ;;
    *)
        gateway="10.1.10.11"
  esac
  echo "Gateway is ${gateway}"
  if [ "${1}" == "connect" ]; then

    cat << EOF | sudo tee ${NW_CONF} > /dev/null
### updated by script ~/ubuntu-client-network.sh connect
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
      dhcp4-overrides:
        use-dns: false
    ens6:
      dhcp4: true
      dhcp4-overrides:
        use-routes: false
        use-dns: false
      nameservers:
        addresses: [${RESOLVER}]
      routes:
        - to: 0.0.0.0/0
          via: ${gateway}
        - to: 3.122.124.33/32
          via: 10.1.1.1
EOF
  sudo iptables -t nat -A PREROUTING -d ${mgmt_ip} -p tcp --dport 443 -j DNAT --to-destination ${traffic_ip}

  elif [ "${1}" == "disconnect" ]; then
    cat << EOF | sudo tee "${NW_CONF}" > /dev/null
### updated by script ~/ubuntu-client-network.sh disconnect
network:
  version: 2
  ethernets:
    ens5:
      dhcp4: true
      dhcp4-overrides:
        use-dns: false
      nameservers:
        addresses: [127.0.0.1]
    ens6: {}
EOF
  sudo iptables -t nat -D PREROUTING -d ${mgmt_ip} -p tcp --dport 443 -j DNAT --to-destination ${traffic_ip}

  else 
    echo "usage: ${BASH_SOURCE[0]} connect|disconnect"
    exit 0 
  fi
  sudo netplan apply
}


# end of script:
# this enables main function to be at top of file

export command="$0 $*"
main "$@"; exit
