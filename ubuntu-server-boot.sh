#! /bin/bash

# to install: curl -OLs https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/ubuntu-server-boot.sh; bash /home/ubuntu/ubuntu-server-boot.sh;

REPO="https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/"
INSTALL_DIR="/home/ubuntu/install-scripts/"
THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename $BASH_SOURCE)"
NGINX_CONF="/etc/nginx/sites-enabled/server.conf"

touch /home/ubuntu/.sudo_as_admin_successful
sudo chown ubuntu /home/ubuntu/.sudo_as_admin_successful
sudo chgrp ubuntu /home/ubuntu/.sudo_as_admin_successful
sudo chmod 755 ${THIS_SCRIPT}

if [ $(whoami) != "ubuntu" ]; then
  echo "switching from $(whoami) to ubuntu"
  echo "please re-run script"
  sudo chown ubuntu:ubuntu ${THIS_SCRIPT}

  # automatically change to ubuntu user when using webshell
  echo "su - ubuntu" >> ~/.profile

  if [ ! -f /home/ubuntu/.profile-original ]; then
    cp -p /home/ubuntu/.profile /home/ubuntu/.profile-original
  fi
  echo "${THIS_SCRIPT}" >> /home/ubuntu/.profile
  su - ubuntu
else

  if [ ! -z "$(cat /home/ubuntu/.profile | grep ${THIS_SCRIPT})" ]; then
     mv /home/ubuntu/.profile-original /home/ubuntu/.profile
     echo "resetting ~/ubuntu/.profile"
  fi

  #reset if network is configured
  if [ -f ${INSTALL_DIR}ubuntu-server-network.sh ]; then
    ${INSTALL_DIR}ubuntu-server-network.sh disconnect
  fi
  
  mgmt_ip="$(ip addr | grep "inet 10\.1\.1\." | sed "s|^.*inet \(10\.1\.1\.[^/]*\).*$|\1|")"
  component_name="$(curl -Ls metadata.udf/deployment | jq --arg ip "${mgmt_ip}" '.deployment.components[] | select(.mgmtIp == $ip) | .name' | cut -d'"' -f2)"
  echo "${component_name} Boot Script starting"
  sudo hostnamectl set-hostname "${component_name}"

  sudo apt update
  sudo NEEDRESTART_SUSPEND=1 apt install nginx tcpdump jq -y

  mkdir -p ${INSTALL_DIR}
  cd ${INSTALL_DIR}
  curl -OLs "${REPO}ubuntu-server-network.sh"
  curl -OLs "${REPO}ubuntu-update-certs.sh"
  chmod 755 ${INSTALL_DIR}/*.sh

  ${INSTALL_DIR}ubuntu-update-certs.sh
  ${INSTALL_DIR}ubuntu-server-network.sh connect
#  ${INSTALL_DIR}ubuntu-server-network.sh disconnect

  echo "#aliases - source ~/alias.sh" > ~/alias.sh
  echo "alias connect=\"${INSTALL_DIR}ubuntu-server-network.sh connect\"" >> ~/alias.sh
  echo "alias disconnect=\"${INSTALL_DIR}ubuntu-server-network.sh disconnect\"" >> ~/alias.sh
  echo "alias upalias=\"source ~/alias.sh\"" >> ~/alias.sh

  echo "installing NGINX"
  cat << 'EOF' | sudo tee "${NGINX_CONF}" > /dev/null
# created by ubuntu-server-boot.sh
server {
	listen 80;
	location / {
		default_type text/plain;
		return 200 "NGINX $server_name\n$remote_addr:$remote_port-->$server_addr:$server_port\n";
	}

    location /images {
		root /home/ubuntu;
		autoindex on;
	}
}

server {
	listen 443 ssl;
	location / {
		default_type text/plain;
		return 200 "NGINX $server_name\n$remote_addr:$remote_port-->$server_addr:$server_port\n";
	}
	ssl_certificate /etc/ssl/certs/secure-demo.crt;
	ssl_certificate_key /etc/ssl/certs/secure-demo.key;	
}

EOF

  sudo rm /etc/nginx/sites-enabled/default
  sudo systemctl restart nginx

fi
