#!/bin/bash

# to install: curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/boot.sh > /config/boot.sh; nohup bash /config/boot.sh --INSTANCE &

echo "BIG-IP Boot Script starting"
mkdir -p /config/my-config

if [ "$1" = "--data" ]; then
  echo "Configuring as Data Plane instance"
  curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/config-data.sh > /config/my-config/config.sh
  curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/DEV-KEY-DATA > /config/DEV-KEYS
  if [ "$2" = "1" ]; then
    echo "Customising configuration for data1"
    sed -i "s|data.secure-demo.net|data1.secure-demo.net|" /config/my-config/config.sh
    sed -i "s|10\.1\.100\.0/24|10.1.110.0/24" /config/my-config/config.sh
  elif [ "$2" = "2" ]; then
    echo "Customising configuration for data2"
    sed -i "s|data.secure-demo.net|data2.secure-demo.net|" /config/my-config/config.sh
    sed -i "s|10\.1\.10\.11/24|10.1.10.12/24" /config/my-config/config.sh
    sed -i "s|10\.1\.20\.11/24|10.1.20.12/24" /config/my-config/config.sh
    sed -i "s|10\.1\.11\.80|10.1.12.80" /config/my-config/config.sh
    sed -i "s|10\.1\.100\.0/24|10.1.120.0/24" /config/my-config/config.sh
  fi
elif [ "$1" = "--control" ]; then
  echo "Configuring as Control Plane instance"
  curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/config-control.sh > /config/my-config/config.sh
  curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/DEV-KEY-CONTROL > /config/DEV-KEYS
else
  echo "Error: instance type not defined: $1"
  exit 1
fi

echo "WARNING: KEEP SHELL IN FOCUS WHILE EXECUTING!"

curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/preferences.sh > /config/my-config/preferences.sh
curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/startup_script_sol11948.sh > /config/my-config/startup_script_sol11948.sh
curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/licensing-route-test.sh > /config/my-config/licensing-route-test.sh
curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/update-certs.sh > /config/my-config/update-certs.sh
curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/install-irules.sh > /config/my-config/install-irules.sh


chmod 755 /config/my-config/*.sh
/config/my-config/preferences.sh
/config/my-config/startup_script_sol11948.sh

chmod 755 /config/startup
my_startup=$(cat "/config/startup" | egrep "^/config/my-config/startup_script_sol11948.sh")
if [[ -z $my_startup ]]
then 
  echo "/config/my-config/startup_script_sol11948.sh" >> startup 
fi
chmod 555 /config/startup*

curl -Lks https://letsencrypt.org/certs/isrgrootx1.pem > /config/my-config/lets-encrypt-ca.pem
/config/my-config/update-certs.sh

tmsh create ltm profile client-ssl secure-demo-clientssl cert-key-chain replace-all-with { default { cert /Common/secure-demo.pkcs12 key /Common/secure-demo.pkcs12 chain /Common/ca-bundle.crt } }

mkdir -p /config/my-config/irules
/config/my-config/install-irules.sh
/config/my-config/config.sh

echo "Finished"

