#! /bin/bash

# ubuntu-client-wireguard.sh (was install-wg.sh)

WG_CONF="/etc/wireguard/wg0.conf"
WG_SECRET="uBKKzLOFFUKMCB1OHQ/NwKL6vbD9Xmzs5YOf2Ek+DGU="

# in case wg0 is already up
wg-quick down wg0

sudo apt-get update -y
sudo apt-get install wireguard wireguard-tools -y

cat << EOF | sudo tee "${WG_CONF}" > /dev/null
[Interface]
Address = 10.245.0.2/24
PrivateKey = #SECRET-DATA

MTU = 1280

PostUp = iptables -t nat -I POSTROUTING -o ens5 -j MASQUERADE
PostUp = iptables -t nat -I POSTROUTING -o ens6 -j MASQUERADE
PostUp = iptables -P FORWARD ACCEPT
#PostUp = systemctl restart dnsmasq
PreDown = iptables -t nat -D POSTROUTING -o ens5 -j MASQUERADE
PreDown = iptables -t nat -D POSTROUTING -o ens6 -j MASQUERADE
PreDown = iptables -P FORWARD DROP

[Peer]
PublicKey = EAbtiYxRZkKnW2CCKDnmMnXF7+RPE5B1Xr6f7/uBMHY=
Endpoint = wireguard.secure-demo.net:1820
AllowedIPs = 10.245.0.0/24, 10.6.0.0/24, 10.0.0.0/24, 10.0.1.0/24
PersistentKeepalive = 25

EOF

sudo sed -i "s%#SECRET-DATA%${WG_SECRET}%" /etc/wireguard/wg0.conf
wg-quick up wg0


