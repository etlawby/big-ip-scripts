#! /bin/bash

# iRules installation script
# /config/my-config/install-irules.sh

install_rule () {
   IRULE=$1
   echo "ltm rule $IRULE {" > /config/my-config/irules/$IRULE
   curl -s https://gitlab.com/etlawby/irules/-/raw/master/$IRULE >> /config/my-config/irules/$IRULE
   echo >> /config/my-config/irules/$IRULE
   echo "}" >> /config/my-config/irules/$IRULE
   tmsh load sys config file /config/my-config/irules/$IRULE merge
}

install_rule debug-clientssl-irule
install_rule debug-dhcp-irule
install_rule debug-dns-irule
install_rule debug-event-irule
install_rule debug-http-irule
install_rule debug-ip-irule
install_rule debug-radius-irule
install_rule debug-serverssl-irule
install_rule debug-sip-irule
install_rule debug-tcp-irule
install_rule debug-udp-irule
install_rule http-respond-irule
install_rule internet-monitor-irule

tmsh save /sys config


