#! /bin/bash

# Licensing Server Route Test Script

DEV_SERVER="authem.f5net.com"
PROD_SERVER="activate.f5.com"

check_route () {
   fqdn=$1
   ip=$2
   if [ -z $ip ]; then
      echo "DNS lookup for $fqdn failed"
   else
      echo "$fqdn = $ip"
      ping=$(ping $fqdn -c 3 | tail -n 2 | head -n 1 | egrep "100% packet loss")
      if [ -z "$ping" ]; then
         echo "Route to $fqdn is OK"
      else
         echo "Route to $fqdn FAILED"
      fi
   fi
}

ip_dev_server=$(dig +short $DEV_SERVER)
ip_prod_server=$(dig +short $PROD_SERVER)

echo "Starting licensing route check"
check_route $DEV_SERVER $ip_dev_server
check_route $PROD_SERVER $ip_prod_server

echo "adding route $DEV_SERVER"
tmsh create /sys management-route TEMPORARY-DEV-LICENSE_SERVER_ROUTE network $ip_dev_server/32 gateway 10.1.1.1
echo "adding route $PROD_SERVER"
tmsh create /sys management-route TEMPORARY-PROD-LICENSE_SERVER_ROUTE network $ip_prod_server/32 gateway 10.1.1.1

check_route $DEV_SERVER $ip_dev_server
check_route $PROD_SERVER $ip_prod_server

echo "deleting route $DEV_SERVER"
tmsh delete /sys management-route TEMPORARY-DEV-LICENSE_SERVER_ROUTE
echo "deleting route $PROD_SERVER"
tmsh delete /sys management-route TEMPORARY-PROD-LICENSE_SERVER_ROUTE

check_route $DEV_SERVER $ip_dev_server
check_route $PROD_SERVER $ip_prod_server
echo "finished"
