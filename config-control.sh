#! /bin/bash

# config for cp1.secure-demo.net

tmsh modify /sys global-settings hostname control.secure-demo.net 

tmsh create /net vlan subscriber-vlan interfaces replace-all-with { 1.1 }
tmsh create /net vlan internet-vlan interfaces replace-all-with { 1.2 }
tmsh create /net self subscriber-self address 10.1.10.21/24 vlan subscriber-vlan 
tmsh create /net self internet-self address 10.1.20.21/24 vlan internet-vlan
tmsh create /net route default-tmm-route network default gw 10.1.20.1

tmsh create /net route dp1-internal-route network 10.1.11.0/25 gw 10.1.10.11
tmsh create /net route dp1-external-route network 10.1.11.128/25 gw 10.1.20.11
tmsh create /net route server-route network 10.1.101.0/24 gw 10.1.20.101

# install new images
# scp -i /config/ssh/ssh_host_rsa_key ubuntu@10.1.1.8:BIGIP-* /shared/images
# tmsh modify /sys disk directory /shared new-size 31457280
# tmsh save /sys config
# tmsh show /sys disk   
# tmsh reboot

# tmsh install /sys software image BIGIP-15.1.8-0.0.7.iso volume HD1.2 create-volume 
# tmsh install /sys software image BIGIP-16.1.3.2-0.0.4.iso volume HD1.3 create-volume 
# tmsh install /sys software image BIGIP-17.0.0.1-0.0.4.iso volume HD1.4 create-volume

# tmsh modify /ltm default-node-monitor rule icmp 

tmsh create /ltm node google-dns-1-node address 8.8.4.4
tmsh create /ltm node google-dns-2-node address 8.8.8.8
tmsh create /ltm monitor dns dns-google-monitor defaults-from dns qname www.google.com
tmsh create /ltm pool google-dns-pool members replace-all-with { google-dns-1-node:53 google-dns-2-node:53 } monitor dns-google-monitor 
tmsh create /ltm pool internet-monitor-pool members replace-all-with { google-dns-1-node:0 google-dns-2-node:0 } monitor gateway_icmp
tmsh create /ltm virtual http-respond-vs destination 10.1.21.80:80 profiles add { http } rules { http-respond-irule internet-monitor-irule }

# tmsh create /ltm node self-mgmt-node address 10.1.1.245
# tmsh create /ltm pool self-monitor-pool members replace-all-with { self_mgmt_node:0 } monitor gateway_icmp

tmsh save /sys config
