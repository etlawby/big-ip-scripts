#! /bin/bash

# Generic BIG-IP preferences

tmsh modify auth password-policy policy-enforcement disabled
# echo -e "admin\nadmin" | tmsh modify auth password admin; history -d $(history 1)
echo -e "admin\nadmin" | tmsh modify auth password admin
tmsh modify /sys management-dhcp sys-mgmt-dhcp-config request-options delete { host-name domain-name-servers}
tmsh modify /sys httpd auth-pam-idle-timeout 2419200
tmsh modify /sys httpd redirect-http-to-https enabled 
tmsh modify /sys ntp servers replace-all-with { pool.ntp.org }
tmsh modify /sys ntp timezone Europe/London
tmsh modify /sys dns name-servers replace-all-with { 10.1.1.2 }
tmsh modify /sys db ui.is24hour value true
tmsh modify /sys db ui.system.preferences.advancedselection value advanced
tmsh modify /sys db ui.system.preferences.recordsperscreen value 100
tmsh modify /sys db ui.system.preferences.startscreen value virtual_servers
tmsh modify /sys db ui.system.preferences.statisticsformat value unformatted
tmsh modify /sys db tmm.cec.classifier.abr.enable value true
tmsh modify /sys db tm.rstcause.log value enable
tmsh modify /sys db tm.rstcause.pkt value disable
tmsh modify /sys db setup.run value false

# tmsh modify /sys dns name-servers replace-all-with { 8.8.8.8 8.8.4.4 }
# use name-servers { 10.1.1.2 } in UDF for DEV licences
#tmsh modify /sys dm tm.mvenable value enable
#tmsh modify /sys global-settings hostname secure-demo.net
#tmsh modify /sys snmp allowed-addresses add {10.1.1.1/255.255.255.0}

tmsh save /sys config



