#! /bin/bash

# to install: curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/boot-client1.sh > /home/ubuntu/boot.sh; bash /home/ubuntu/boot.sh

THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename $BASH_SOURCE)"

touch /home/ubuntu/.sudo_as_admin_successful
sudo chown ubuntu /home/ubuntu/.sudo_as_admin_successful
sudo chgrp ubuntu /home/ubuntu/.sudo_as_admin_successful
sudo chmod 755 $THIS_SCRIPT

if [ $(whoami) != "ubuntu" ]; then
  echo "switching from $(whoami) to ubuntu"
#  echo "please re-run script"
  sudo chown ubuntu $THIS_SCRIPT
  sudo chgrp ubuntu $THIS_SCRIPT

  # automatically change to ubuntu user when using webshell
  echo "su - ubuntu" >> ~/.profile
  
  if [ ! -f /home/ubuntu/.profile-original ]; then
    cp -p /home/ubuntu/.profile /home/ubuntu/.profile-original
  fi
  echo "$THIS_SCRIPT" >> /home/ubuntu/.profile
  su - ubuntu
else
  echo "Client1 Boot Script starting"
  
  if [ ! -z "$(cat /home/ubuntu/.profile | egrep $THIS_SCRIPT)" ]; then
     mv /home/ubuntu/.profile-original /home/ubuntu/.profile
     echo "resetting ~/ubuntu/.profile"
  fi

  sudo sh -c "echo client1 > /etc/hostname"
  sudo sh -c "curl -Ls https://gitlab.com/etlawby/big-ip-scripts/-/raw/main/udf-client1.yaml >/etc/netplan/udf-client1.yaml"

  # perform this last as will disconnect shell
  echo "rebooting - please wait..."
  sudo netplan apply
  sudo reboot

fi
