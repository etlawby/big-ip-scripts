#! /bin/bash

host=$(curl http://metadata.udf/deployment -s | jq '.deployment.components[]|select(.name=="server1").accessMethods.ssh[].host'| tr -d '"')
port=$(curl http://metadata.udf/deployment -s | jq '.deployment.components[]|select(.name=="server1").accessMethods.ssh[].port')
user=$(curl http://metadata.udf/deployment -s | jq '.deployment.components[]|select(.name=="server1").accessMethods.ssh[].parameters.username'| tr -d '"')

echo "use following command on image server to deploy images:"
echo "scp -P $port /home/ubuntu/images/* $user@$host:images/."
