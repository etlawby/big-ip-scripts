#! /bin/bash

# SSL Certs update script
# /home/ubuntu/ubuntu-update-certs.sh (was update-certs-ubuntu.sh)

#CREDS="admin:guwpug50-H"
CREDS="machine certs.secure-demo.net login certificate-access password bnjedglm"
CREDS_DIR="/etc/ssl/certs/"
CERTS_REPO="https://certs.secure-demo.net/"
CERTS_DIR="/etc/ssl/letsencrypt/live/"

CREDS_FILE="${CREDS_DIR}.netrc"

sudo mkdir -p "${CREDS_DIR}"
echo "${CREDS}" | sudo tee "${CREDS_FILE}" > /dev/null

echo "Checking certificates..."
sudo mkdir -p "${CERTS_DIR}secure-demo.net"
sudo mkdir -p "${CERTS_DIR}telco-xc.com"
curl -Ls "${CERTS_REPO}secure-demo/crt" --netrc-file "${CREDS_FILE}" | sudo tee "${CERTS_DIR}secure-demo.crt.latest" > /dev/null
sudo touch "/etc/ssl/certs/secure-demo.crt"
if [ ! -z "$(diff ${CERTS_DIR}secure-demo.crt.latest /etc/ssl/certs/secure-demo.crt)" ]; then
  echo "Updating certificates..."
  curl -Ls "${CERTS_REPO}secure-demo/crt" --netrc-file "${CREDS_FILE}" | sudo tee "${CERTS_DIR}secure-demo.net/fullchain.pem" > /dev/null
  curl -Ls "${CERTS_REPO}secure-demo/key" --netrc-file "${CREDS_FILE}" | sudo tee "${CERTS_DIR}secure-demo.net/privkey.pem" > /dev/null
  curl -Ls "${CERTS_REPO}telco-xc/crt" --netrc-file "${CREDS_FILE}" | sudo tee "${CERTS_DIR}telco-xc.com/fullchain.pem" > /dev/null
  curl -Ls "${CERTS_REPO}telco-xc/key" --netrc-file "${CREDS_FILE}" | sudo tee "${CERTS_DIR}telco-xc.com/privkey.pem" > /dev/null
  sudo ln -sf "${CERTS_DIR}secure-demo.net/fullchain.pem" /etc/ssl/certs/secure-demo.crt
  sudo ln -sf "${CERTS_DIR}secure-demo.net/privkey.pem" /etc/ssl/certs/secure-demo.key
  sudo chmod 600 "${CERTS_DIR}secure-demo.net/privkey.pem"
  sudo chmod 600 "${CERTS_DIR}telco-xc.com/privkey.pem"
  sudo systemctl reload nginx
else
  echo "certificates unchanged"
fi

THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename $BASH_SOURCE)"

crontab -l > my_crontab_file
cat my_crontab_file | egrep -v "$THIS_SCRIPT" > my_crontab2_file
mv my_crontab2_file my_crontab_file
echo "30 4 * * * $THIS_SCRIPT" >> my_crontab_file
crontab my_crontab_file
rm my_crontab_file

