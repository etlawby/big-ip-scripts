#! /bin/bash

# SSL Certs update script
# /config/my-config/update-certs.sh

#CREDS="admin:guwpug50-H"
CREDS="certificate-access:bnjedglm"

echo "Checking certificates..."
curl -Lsu $CREDS --cacert /config/my-config/lets-encrypt-ca.pem https://certs.secure-demo.net/secure-demo/crt > /config/my-config/secure-demo.crt.latest
touch /config/my-config/secure-demo.crt
if [ ! -z "$(diff /config/my-config/secure-demo.crt.latest /config/my-config/secure-demo.crt)" ]; then
   echo "Updating certificates..."
   curl -Lsu $CREDS --cacert /config/my-config/lets-encrypt-ca.pem https://certs.secure-demo.net/secure-demo/crt > /config/my-config/secure-demo.crt
   curl -Lsu -$CREDS --cacert /config/my-config/lets-encrypt-ca.pem https://certs.secure-demo.net/secure-demo/key > /config/my-config/secure-demo.key
   curl -Lsu $CREDS --cacert /config/my-config/lets-encrypt-ca.pem https://certs.secure-demo.net/secure-demo/pfx > /config/my-config/secure-demo.pfx
   /bin/cp /config/my-config/secure-demo.crt /config/httpd/conf/ssl.crt/server.crt
   /bin/cp /config/my-config/secure-demo.key /config/httpd/conf/ssl.key/server.key
   echo "restarting httpd"
   tmsh restart sys service httpd
   tmsh install sys crypto pkcs12 secure-demo.pkcs12 from-local-file /config/my-config/secure-demo.pfx
   tmsh save /sys config
else
   echo "certificates unchanged"
fi

THIS_SCRIPT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename $BASH_SOURCE)"

crontab -l > my_crontab_file
cat my_crontab_file | egrep -v "$THIS_SCRIPT" > my_crontab2_file
cp my_crontab2_file my_crontab_file
echo "30 4 * * * $THIS_SCRIPT" >> my_crontab_file
crontab my_crontab_file
rm my_crontab_file

