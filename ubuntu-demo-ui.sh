#! /bin/bash

# ubuntu-demo-ui.sh

echo "updating demo-ui..."

cd ~
rm -rf demo-ui/

git clone https://gitlab.com/etlawby/demo-ui.git

sudo mkdir -p /etc/nginx/sites-available
sudo cp demo-ui/etc-nginx-sitesavailable/* /etc/nginx/sites-available
sudo ln -sf /etc/nginx/sites-available/* /etc/nginx/sites-enabled

sudo mkdir -p /usr/share/nginx/html/demo
sudo cp -r demo-ui/usr-share-nginx-html-demo/* /usr/share/nginx/html/demo

sudo mkdir -p /usr/share/nginx/cgi-bin
sudo cp demo-ui/usr-share-nginx-cgibin/* /usr/share/nginx/cgi-bin

sudo mkdir -p /usr/share/nginx/cgi-bin/demo
sudo cp -r demo-ui/usr-share-nginx-cgibin-demo/* /usr/share/nginx/cgi-bin/demo

sudo systemctl reload nginx